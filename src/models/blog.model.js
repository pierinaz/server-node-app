let mongoose = require('mongoose');
/*let conn = require('constants');
mongoose.connect(`mongodb+srv://${conn.user}:${conn.password}@${conn.server}/${conn.database}?retryWrites=true`)*/

mongoose.connect('mongodb+srv://admin:admin123@cluster0-rk0e4.gcp.mongodb.net/blogs?retryWrites=true')

let BlogSchema = new mongoose.Schema({
    author: String,
    created_at: String,
    story_id: Number,
    story_title: String,
    story_url: String,
    title: String,
    url: String
})

module.exports = mongoose.model('Blog', BlogSchema)