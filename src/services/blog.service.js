const request = require("request")
const ApiBaseurl = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs'

const service = {

  async getBlogs() {
    const options = {
      method: "GET",
      url: `${ApiBaseurl}`,
      headers: {
        "Content-Type": "application/json"
      }
    }
    return await new Promise((resolve, reject) => {
      request(options, (err, res, body) => {
        if (err) reject(err)
        resolve(JSON.parse(body))
      })
    })
  }
}

module.exports = service
