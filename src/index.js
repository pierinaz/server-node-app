let express = require('express');
let path = require('path');
let bodyParser = require('body-parser');

let app = express();

let blogRoute = require('./routes/blog');

app.use(bodyParser.json());
app.use(blogRoute);
app.use(express.static('public'))

// Configurations for headers and cors
app.use((req, res) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

//Handler for error 404
app.use((req, res) => {
    res.status(404).send('We think you are lost!')
})

//Handler for error 500
app.use((err, req, res, next) => {
    res.send("Oh uh, something went wrong" + err);
    //res.sendFile(path.join(__dirname, '../public/500.html'));
})

const PORT = process.env.PORT || 3000

app.listen(PORT, () => console.info(`Server has started ${PORT}`));