let blogModel = require('../models/blog.model')
let blogService = require('../services/blog.service')
let express = require('express')
let router = express.Router()

router.get('/blogs', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    blogModel.find().then(doc => {
        res.json(doc)  
    }).catch(err => {
        res.status(500).json(err)
    })
})

router.get('/blogs/:populate', (req, resp) => {
    if(req.params.populate === 'populate'){
        blogService.getBlogs().then((res) => {
            let blogs = res.hits;
            for(let i=0; i<blogs.length; i++){
            let model = new blogModel({ 
                author: blogs[i].author,
                created_at: blogs[i].created_at,
                story_id: blogs[i].story_id,
                story_title: blogs[i].story_title,
                story_url: blogs[i].story_url,
                title: blogs[i].title,
                url: blogs[i].url  });

                model.save().then(doc => {
                    if(!doc || doc.length === 0) return res.status(500).send(doc);
                }).catch(err => { res.status(500),json(err) })
            } 
            resp.status(200).send("Process complete. Data was load.")
        }).catch(err => {
            resp.status(500).json(err)
        })

    } else {       
        return resp.status(400).send('Missing parameter: populate');
    }
})

router.delete('/blog', (req, res, next) => {
    if(req.query.id){
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'application/json');
        
        blogModel.findOneAndRemove({
            _id: req.query.id
        }, (err, doc, res) => { if(err) res.status(500).json(err) })
        .then(doc => {
            res.send(doc)
            next(doc)
        }).catch(err => {
            res.status(500).json(err)
        })
    } else {
        res.status(500).json("Missing required param")
    }
    
})

module.exports = router
